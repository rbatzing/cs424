<img src="./image/bigdata2.png" align="center" width="25%">

# CS424 Big Data Software

## Sex ratio changes

* 1950 Sex ratios at birth in 1950:
![1950](./image/sexratio1950.png)

* 2010 Sex ratios at birth in 2010:
![2010](./image/sexratio2010.png)

